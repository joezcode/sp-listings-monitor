## Skinport.com web socket

A web socket exercise listening to new listings on Skinport (Counter Strike skins).

Deployed here:
https://joezcode.gitlab.io/sp-listings-monitor
