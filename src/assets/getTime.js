// Timestamp.js

const getCurrentTime = () => {
    
    // Get current time
    let time = Date.now();

    // Convert to human-friendly time    
    time = new Intl.DateTimeFormat('en-UK', {year: 'numeric', month: '2-digit',day: '2-digit', hour: '2-digit', minute: '2-digit', second: '2-digit'}).format(time);

    return time;

}


const getTimeLeft = (time) => {
    
    // Get current time
    time = Date.parse(time) - (Date.now())
    
    // Convert to human-friendly time    
    const timeLeft = Math.ceil((((time / 1000) / 60) / 60) / 24)

    return timeLeft;
}

export { getCurrentTime, getTimeLeft }

    