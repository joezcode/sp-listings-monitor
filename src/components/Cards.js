// Dependencies
// import { useEffect } from 'react';
import Card from './CardItem';
import { Box, SpaceBetween, A  } from './GlobalStyles';
import styled from 'styled-components';
// import useSessionStorage from './useSessionStorage';

// Styled components
const DisplayCards = styled.div`
    display: grid;
    grid-template-columns: repeat(auto-fill, minmax(220px, 1fr));
    border: 2px solid rgba(106,108,110,0.2);
`;

// App
function Cards({data}) {
    
    // Sorting
    // const [sortType, setSortType] = useSessionStorage([], "sortType")
    // const [filterType, setFilterType] = useSessionStorage([], "filterType")

    // useEffect(() => {
    //     const filterArray = category => {
    //         console.log(filterType)
    //         data = data.filter(item => filterType.includes(item.category));
    //     };
    //     filterArray(filterType);



    // }, [filterType]); 

    // const handleFilter = (e, value) => {

    //     if (e.target.checked) {
    //         setFilterType(filterType => [...filterType, value])
    //     } else {
    //         setFilterType(filterType.filter(val => val !== value))
    //     }
    // }

	return (
        <Box>
            <SpaceBetween>
                <h1>Listings Monitor</h1> 
                <Box>
                    <A onClick={sessionStorage.clear()}>Clear session storage... </A>
                    <A href="/sp-listings-monitor"> and then refresh</A>
                    </Box>
            </SpaceBetween>
            {/* <Box>
                <div className="checkbox">
                    <label>
                        <input
                            type="checkbox"
                            value="Knife"
                            onClick={(e) => handleFilter(e, e.target.value)}
                        />
                        Knife
                    </label>
                </div>
                <div className="checkbox">
                    <label>
                        <input
                            type="checkbox"
                            value="Rifle"
                            onClick={(e) => handleFilter(e, e.target.value)}
                        />
                        Rifle
                    </label>
                </div>
                <div className="checkbox">
                    <label>
                        <input
                            type="checkbox"
                            value=""
                            onClick={(e) => setFilterType(e.target.value)}
                        />
                        Clear
                    </label>
                </div>
            </Box> */}
            <DisplayCards>
                {data && data.map((item, index) => {
                    return (
                        <Card 
                            key={index}
                            {...item}
                        />
                    )
                })}
            </DisplayCards>
        </Box>
	);

}

export default Cards;
