// Dependencies
import styled from 'styled-components';

// Styled components
const Item = styled.div`
    position: relative;
    display: flex;
    flex-wrap: wrap;
    align-items: top;
    justify-content: left;

    border: 2px solid ${props => props.border};
    padding: 15px;

    img {
        width: 100%;
    }
`;
const Bar = styled.div`
    position: absolute;
    bottom: 0.5%;
    left: 1%;
    
    width: 98%;
    height: 1.5em;
    background: ${props => props.background};
    text-align: center;
    opacity: 0.9;
`
const Title = styled.h3`
    float: left;
    margin-right: 4px;
    width: 100%;
`
const Name = styled.h2`
`
const Price = styled.h1`
    font-size: 1.5rem;
    width: 100%;
`
const Label = styled.p`    
    font-size: 0.8rem;
    font-weight: 700;
    color: #FFD7000;
`

const Field = styled.p`
    width: 100%;
    font-size: 0.9rem;
    opacity: 0.8;
`
const Hidden = styled.p`
    visibility: hidden;
`

export { Item, Bar, Title, Name, Price, Label, Field, Hidden }