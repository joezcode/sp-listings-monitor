// Dependencies
import { useEffect, useState } from 'react';

// useStickyState   
const useSessionStorage = (defaultValue, key) => {

        // Pairing useState with sessionStorage
        const [value, setValue] = useState(() => {
           const sessionValue = window.sessionStorage.getItem(key);
             return sessionValue !== null
               ? JSON.parse(sessionValue)
               : defaultValue;
        });
        
        // Save value to sessionStorage when setValue has been used to change value
        useEffect(() => {
            window.sessionStorage.setItem(key, JSON.stringify(value));
        }, [key, value]);

        return [value, setValue];
}

export default useSessionStorage;

    
