// Dependencies
import { useEffect, useRef } from 'react';
import { io } from 'socket.io-client';
import { getCurrentTime } from '../assets/getTime';
import useSessionStorage from './useSessionStorage';

// // useStream   
const useStream = () => {

    // The data is stored in useState before being pushed to localStorage through the useSession hook
    const [dataState, setDataState] = useSessionStorage([], "dataState");
    const [saleIds, setSaleIds] = useSessionStorage([], "saleIds")

    // Tracking if we are subscribed to the API
    const isApiConnnected = useRef(false)

    useEffect(() => {

        // Making the websocket
        let url = 'https://skinport.com';
        const socket = io(url, {transports: ['websocket']});

        
        // Make only one connection at a time and log the connect
        socket.on("connect", () => {
            isApiConnnected.current = true;
            console.log(`${getCurrentTime()} | isApiConnnected: ${isApiConnnected.current}`)
            console.log(`${getCurrentTime()} | Connected to ${url} with socket id: ${socket.id}.`)
        });

        // On received messages from the feed
        socket.on('saleFeed', (data) => {
            // data = [{},{},{}...] 
            console.log(`${getCurrentTime()} | New listings received: ${data.sales.length} ...`);
            
            // Making sure we don't get any unwanted categories 
            let unwantedCategories = ["Container", "Graffiti", "Music Kit", "Agent", "Sticker"]

            // Specify the data we want from: {eventType: 'listed', sales: Array(n)}
            for (let item of data.sales) {
                if (unwantedCategories.includes(item.category)) {
                    console.log("Skipping unwanted category: " + item.category)
                    continue
                }
                
                if (saleIds.includes(item.saleId)) {
                    console.log("Dupliace listing found. Skipping...")
                    continue
                }

                setSaleIds(saleIds => [item.saleId, ...saleIds]);
                setDataState(dataState => [item, ...dataState]);
            }
        });


        // Submit required parameters
        socket.emit('saleFeedJoin', {currency: 'EUR', locale: 'en', appid: 730});

        // Log messages on disconnect, close or error.
        socket.on("disconnect", () => {console.log(`${getCurrentTime()} | Disconnected from socket.`)});
        socket.on("close", () => {console.log(`${getCurrentTime()} | Connection to socket closed.`)});
        socket.on("error", (err) => {console.log(`${getCurrentTime()} | Error due to ${err.message}.`)});
        
        
        return () => {            
            isApiConnnected.current = false
            console.log(`${getCurrentTime()} | isApiConnnected: ${isApiConnnected.current}`)
        }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    console.log(`${getCurrentTime()} | Listings updated. Total items: ${dataState.length}.`)
    return dataState;
    
}
export { useStream };
