// Dependencies
import { getTimeLeft } from '../assets/getTime';
import { IoMdTimer } from 'react-icons/io'
import { Item, Bar, Title, Name, Price, Label, Field, Hidden } from './CardItemStyled';


// App
function Card(props) {

    function checkImage(image, assetId) {
        var img = new Image();
        img.src = `https://community.cloudflare.steamstatic.com/economy/image/${image}`;
        img.onerror = () => {
            console.log(`Error loading image error for ${assetId}`);
            img = new Image();
            img.src = `https://cdn.skinport.com/images/screenshots/${assetId}/playside_512x384.png`;
        }
        return img.src;
    }

    if (props) {
        
        // Field formatting
        let itemPrice = (props.salePrice / 100).toFixed(2);
        let itemSuggestedPrice = (props.suggestedPrice / 100).toFixed(2);

        // Highlighters
        const setBorder = () => {
            if (itemPrice > 800) {
                return "palevioletred"
            } else if (itemPrice > 1500) {
                return "red"
            } else {
                return "rgba(106,108,110,0.2)"
            }
        }

        const setBackground = () => {
            if (props.rarity === "Uncommon Grade") {
                return "#6596e2"
            } else if (props.rarity === "Mil-Spec Grade") {
                return "#4d69cd"
            } else if (props.rarity === "Restricted") {
                return "#8947ff"
            } else if (props.rarity === "Classified ") {
                return "#d42be6"
            } else if (props.rarity === "Covert") {
                return "#eb4b4b"
            } else if (props.rarity === "Extraordinary") {
                return "#caab05"
            } else {
                return "rgba(106,108,110,0.5)"
            }
        }

        return (

            <Item border={setBorder()}>

                    {props.lock 
                    ? <Label><IoMdTimer /> in {getTimeLeft(props.lock)} days</Label>
                    : <Label>Tradable</Label>}
                    
                    <img src={checkImage(props.image, props.assetId)} alt={props.marketHashName} />
                    
                    <Price>€ {itemPrice}</Price>
                    <Field>Suggested price: € {itemSuggestedPrice}</Field>
                    
                    <Title>{props.title}</Title>
                    <Name>{props.name}</Name>
                    <Field>{props.text}</Field>
                    
                    {props.wear && <Field>{(props.wear).toFixed(6)} wear</Field>}
                    (<a href={props.link}>Open in-game</a>)

                    <Hidden>Sale ID: {props.saleId}</Hidden>
                    <Hidden>Category: {props.category}</Hidden>
                    <Hidden>Sub-category: {props.subCategory}</Hidden>

                    <Bar background={setBackground()}>{props.rarity}</Bar>

            </Item>

        );
    }
}

export default Card;
