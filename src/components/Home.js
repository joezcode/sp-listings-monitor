// Dependencies
import styled from 'styled-components';

// Styled components
const Wrapper = styled.div`
	min-height: 50vh;
	width: 90%;
    margin: 0 auto;
`

// App
function Home() {

	return (
        <Wrapper>
            <h1>Pick a format</h1>
        </Wrapper>
	);

}

export default Home;
