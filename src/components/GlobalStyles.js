// Dependencies
import styled from 'styled-components';

// Styled components
const Wrapper = styled.div`
	width: 90%;
    margin: 5% auto;

`
const Box = styled.div`
`
const SpaceBetween = styled.div`
    display: flex;
    justify-content: space-between;
    width: 100%;
    margin: 2% auto;
`
const A = styled.a`
    cursor: pointer;

    &:hover {
        color: pink;
    }

    &:active {
        color: white;
    }
`
export { Wrapper, Box, SpaceBetween, A }