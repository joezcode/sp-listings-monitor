// Dependencies
// import { useState } from 'react';
import { DataGrid } from '@mui/x-data-grid';
import styled from 'styled-components';

// Styled components
const Items = styled.div`
    height: 500px;
        height: 70vh;
    }
`;

// App
function ItemTable({data}) {

    // const [sortModel, setSortModel] = useState([
    //     {
    //       field: 'saleId',
    //       sort: 'desc',
    //     },
    //   ]);
	
	const columns = [
		{ field: 'time', headerName: 'Time', width: 180 },
		{ field: 'marketName', headerName: 'Name', width: 300 },
		{ field: 'salePrice', headerName: 'Price', width: 100 },
		{ field: 'suggestedPrice', headerName: 'Suggested Price', width: 150 },
		{ field: 'exterior', headerName: 'exterior', width: 150 },
		{ field: 'wear', headerName: 'wear', width: 150 },
		{ field: 'pattern', headerName: 'pattern', width: 150 },
		{ field: 'saleId', headerName: 'Sale ID', width: 100 },
		{ field: 'steamid', headerName: 'Steam ID', width: 300 },
		{ field: 'link', headerName: 'link', width: 300 },
	];

	return (

        <Items>
            <h1>Listings Monitor</h1>
            <DataGrid 
            rows={data} 
            columns={columns} 
            // sortModel={sortModel}
            // onSortModelChange={(model) => setSortModel(model)}
            />
        </Items>

	);

}

export default ItemTable;
