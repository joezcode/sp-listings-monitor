// Dependencies
import { React }from 'react';
import { NavLink } from "react-router-dom";
import { Switch as DarkModeSwitch } from "@mui/material";
import { SpaceBetween, Box } from './GlobalStyles';
import styled from 'styled-components';

// Styled components
const NavBar = styled.div`
    position: sticky;
    height: 100px;
    width: 100%;

`
const Menu = styled.nav`

    a {
        text-decoration: none;
        
        &:hover {
            color: pink;
        }
    
        &:active {
            color: white;
        }
    }
`

// App
const Header = ({ checked, onChange}) => {

	return (
        <NavBar>
            <SpaceBetween>

                <Menu>
                    <span>Data format: </span>
                    {/* <NavLink to="/table">
                        Table
                    </NavLink>
                    <span> / </span> */}
                    <NavLink to="/cards">
                        Cards
                    </NavLink>
                </Menu>

                <Box> 
                    <span>{(checked === true) ? "Turn on the light" : "Turn off the light"}</span>
                    <DarkModeSwitch checked={checked} onChange={onChange} />
                </Box>

            </SpaceBetween>
        </NavBar>
	);
};

export default Header;
