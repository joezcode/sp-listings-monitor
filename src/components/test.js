/* Dependencies */
import React from 'react';


const data = [
    {
        "_id": {
            "$oid": "623de762e76debb642a3e710"
        },
        "data": {
            "videos": [
                {
                    "run": 1,
                    "response": 200,
                    "time": "0.0048"
                },
                {
                    "run": 2,
                    "response": 200,
                    "time": "0.0036"
                },
                {
                    "response": 200,
                    "time": "0.0041"
                },
                {
                    "response": 200,
                    "time": "0.0035"
                },
                {
                    "response": 200,
                    "time": "0.0034"
                }
            ],
            "root": [
                {
                    "response": 200,
                    "time": "0.0032"
                },
                {
                    "response": 200,
                    "time": "0.0025"
                },
                {
                    "response": 200,
                    "time": "0.0021"
                }
            ],
            "perfresults": [
                {
                    "response": 200,
                    "time": "0.0035"
                },
                {
                    "response": 200,
                    "time": "0.0037"
                }
            ]
        }
    }
]

function App() {

    const dataLevel = data.map(({ _id, videos, root, perfresults}) => {
        videos.map(({response, time}) => {
          return (
              <Wrapper>
                  <h2>{_id}</h2>
                  <h2>{response}</h2>
                  <h2>{time}</h2>
              </Wrapper>
          );
        });
    });


	return (
		<div className="App">
			<Header />
			<div className="Content">
				<Home />
				<Projects />
				<About />
				<Contact />
			</div>
			<Footer />
		</div>
	);
};

export default App;

