// Dependencies
import { React, useState } from 'react';
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { createTheme, ThemeProvider, CssBaseline } from "@mui/material";
import "@fontsource/roboto";
import { useStream } from './components/useStream';


// Components
import Header from './components/Header';
import Home from './components/Home';
import Cards from './components/Cards';
// import Table from './components/Table';
import './App.css';

// Styled components
import { Wrapper } from './components/GlobalStyles';

const App = () => {

    // The dark theme is used by default
    const [isDarkTheme, setIsDarkTheme] = useState(true);

    // Theme setup
    const light = { palette: {mode: 'light'} };
    const dark = { palette: {mode: 'dark'} };

    // This function is triggered when the Switch component is toggled
    const changeTheme = () => {
        setIsDarkTheme(!isDarkTheme);
    };

    // Get data from websocket
    const data = useStream();

	return (     

        <ThemeProvider theme={isDarkTheme ? createTheme(dark) : createTheme(light)} >
            <CssBaseline enableColorScheme />

            <Router basename="/sp-listings-monitor">
                <div className="App">
                    <Wrapper>

                        <Header checked={isDarkTheme} onChange={changeTheme} />
                    
                        <Routes>
                            <Route path="/" element={<Home />} />
                            {/* <Route path="/table" element={<Table data={data} />} /> */}
                            <Route path="/cards" element={<Cards data={data} />} />
                        </Routes>

                    </Wrapper>
                </div>
            </Router>

        </ThemeProvider>
	);
};

export default App;
